# View app details

Look at existing pods.

```
kubectl get pods
```

View containers and images inside pods.

```
kubectl describe pods
```
