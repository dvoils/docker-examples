# Install Kubernetes

+ [Install Docker](install-docker.md)

## Install kubectl
kubectl controls the Kubernetes cluster manager.

## Install kublet and kubeadm
These must be installed on all machines in the cluster, including the master.

* kubelet: the most core component of Kubernetes. It runs on all of the machines in your cluster and does things like starting pods and containers.
* kubeadm: the command to bootstrap the cluster.

Get root
`sudo su`

 run the following script

```
apt-get update && apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
```

## Start the cluster

While still root, initialize the control node

+ `kubeadm init --pod-network-cidr=10.244.0.0/16`

Switch back to user and et up kubectl on the control node. Note, run these commands every time after logging out and back in.

```
sudo cp /etc/kubernetes/admin.conf $HOME/
sudo chown $(id -u):$(id -g) $HOME/admin.conf
export KUBECONFIG=$HOME/admin.conf

```

Install networking layer on the control node

```
git clone https://github.com/coreos/flannel.git
cd flannel/Documentation
kubectl apply -f kube-flannel.yml
```

On all worker nodes

`$ kubeadm join --token <token> <ip:port>`


On the control node

`$ kubectl get nodes`
