sudo cp /etc/kubernetes/admin.conf $HOME/
sudo chown $(id -u):$(id -g) $HOME/admin.conf
export KUBECONFIG=$HOME/admin.conf
git clone https://github.com/coreos/flannel.git
cd flannel/Documentation
kubectl apply -f kube-flannel.yml
