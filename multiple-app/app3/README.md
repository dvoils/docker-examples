
## Build and run container
```
docker build -t name/node-web-app .
docker run -p 49160:8080 -d name/node-web-app
```

## Get container ID
`$ docker ps`

## Print app output
`$ docker logs <container id>`

## Test container
```
curl -i localhost:49160
http://<ip>:49160
```