# Install Docker

`$ sudo apt-get update`

Add the GPG key for the official Docker repository to the system.

`$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

Add the Docker repository to APT sources.

`$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`

Update the package database with the Docker packages from the newly added repo.

`$ sudo apt-get update`

Make sure to install from the Docker repo instead of the default Ubuntu 16.04 repo.

`$ apt-cache policy docker-ce`

Install Docker.

`$ sudo apt-get install -y docker-ce`

Docker should now be installed. Check that it's running.

`$ sudo systemctl status docker`

To avoid typing sudo when running a docker command, add your username to the docker group.

`$ sudo usermod -aG docker ${USER}`

To apply the new group membership, log out of the server and back in, or type:

`$ su - ${USER}`
