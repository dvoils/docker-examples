# Some useful docker commands

## Search for images available on docker hub

+ `docker search <search string>`

## Remove images
+ `docker rmi <image id>`

## View system-wide information about Docker, use:

+ `docker info`

## Search Docker Hub for images.

+ `docker search <image>`

## Download an image from Docker Hub and run it locally.
+ `docker pull <image>`

## Build an image from a Dockerfile
+ `docker build -t <image-name> <path to Dockerfile>`

## List local docker images.
+ `docker images`

## Run an image in the background  
+ `docker run -d -p --name <name> <port>:<port> <image>`
+ `docker run -it -d -p 52022:22 <image>`

## Run an image in the foreground
+ `docker run --rm -it ubuntu:16.10`

## Run docker container interactively on <image>.
+ `docker run -it <image> <command>`

## Copy local file to docker containter.
+ `docker cp <file> <container>:/<file>`
+ `docker cp <container>:/<file> <file>`

## Get an interactive shell in a running <image>.
+ `docker exec -it <image> bash`

## Remove all unused or dangling Images, containers, volumes, and networks
+ `docker system prune -a`

## List all docker volumes
+ `docker volume ls`

## Remove all docker volumes
+ `docker volume prune`

## Stop and remove all containers.
+ `docker stop $(docker ps -a -q)`
+ `docker rm $(docker ps -a -q)`

## Remove all images.

+ `docker rmi -f $(docker images -q)`

## Show all containers including hidden ones
+ `docker ps -a`

## Docker Compose
+ `docker-compose stop <image>`
+ `docker-compose build <image>`
+ `docker-compose up -d --no-recreate <image>`
+ `docker-compose rm -fv mongodb`
