<?php
/*****************************************************************

	File name: browser.php
	Author: Gary White
	Last modified: June 2007, by Luke Wallin
	
	**************************************************************

	Copyright (C) 2003  Gary White
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details at:
	http://www.gnu.org/copyleft/gpl.html

	**************************************************************

	Browser class
	
	Identifies the user's Operating system, browser and version
	by parsing the HTTP_USER_AGENT string sent to the server
	
	Typical Usage:
	
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/browser.php');
		$br = new Browser;
		echo "$br->Platform, $br->Name version $br->Version";
	
	For operating systems, it will correctly identify:
		Microsoft Windows (95,98, ME, 2000, XP, vista, 2003)
		MacIntosh (intel / PPC, Ipod, Iphone)
		Linux (debian, fedora, suse, ubuntu, google)
		Wii
		PSP
		(Yahoo!)

	Anything not determined to be one of the above is considered to by Unix
	because most Unix based browsers seem to not report the operating system.
	The only known problem here is that, if a HTTP_USER_AGENT string does not
	contain the operating system, it will be identified as Unix. For unknown
	browsers, this may not be correct.
	
	For browsers, it should correctly identify all versions of:
		Amaya
		Galeon
		iCab
		Internet Explorer
			For AOL versions it will identify as Internet Explorer (AOL) and the version
			will be the AOL version instead of the IE version.
		Konqueror
		Lynx
		Mozilla
		Netscape Navigator/Communicator
		OmniWeb
		Opera
		Pocket Internet Explorer for handhelds
		Safari
		WebTV
		Firefox
		SeaMonkey
		Dillo
		Vienna
		Chrome
		
		Googlebot
		Slurp
		MsnBot
*****************************************************************/

class browser{

	var $Name = "Unknown";
	var $Version = "Unknown";
	var $Platform = "Unknown";
	var $OSversion = "Unknown";
	var $UserAgent = "Not reported";
	var $AOL = false;

	function browser($usethis){
		$agent = $_SERVER['HTTP_USER_AGENT'];
		
		if($usethis){$agent=$usethis;}
		
		// initialize properties
		$bd['platform'] = "Unknown";
		$bd['browser'] = "Unknown";
		$bd['version'] = "Unknown";
		$bd['OSversion'] = "Unknown";
		$this->UserAgent = $agent;

		// find operating system
		if (eregi("win", $agent))
			{$bd['platform'] = "Windows";
			//try to find version of windows
        if (eregi("NT 6.1", $agent))
			{$bd['OSversion'] = '7';}
			elseif (eregi("NT 6.0", $agent))
			{$bd['OSversion'] = 'Vista';}
			elseif (eregi("NT 5.2", $agent))
			{$bd['OSversion'] = '2003/XP64';}
			elseif (eregi("NT 5.1", $agent))
			{$bd['OSversion'] = 'XP';}
			elseif (eregi("NT 5.0", $agent))
			{$bd['OSversion'] = '2000';}
			elseif (eregi("NT 4.0", $agent) || eregi("NT4.0", $agent))
			{$bd['OSversion'] = 'NT';}
			elseif (eregi("Win 9x 4.90", $agent))
			{$bd['OSversion'] = 'ME';}
			elseif (eregi("Windows 98", $agent) || eregi("win98", $agent))
			{$bd['OSversion'] = '98';}
			elseif (eregi("Windows 95", $agent) || eregi("win95", $agent))
			{$bd['OSversion'] = '95';}}			
			
		elseif (eregi("mac", $agent))
			{$bd['platform'] = "MacIntosh";
			if (eregi("PPC", $agent))
			{$bd['OSversion'] = 'Power PC';}
			elseif (eregi("Intel", $agent))
			{$bd['OSversion'] = 'Intel';}
			elseif (eregi("iPhone", $agent))
			{$bd['OSversion'] = 'iPhone';}
			elseif (eregi("iPod", $agent))
			{$bd['OSversion'] = 'iPod';}
			}
			
		elseif (eregi("linux", $agent))
			{$bd['platform'] = "Linux";
			//try and find linux distros
			if (eregi("Debian", $agent))
			{$bd['OSversion'] = 'Debian';}
			elseif (eregi("Fedora", $agent))
			{$bd['OSversion'] = 'Fedora';}
			elseif (eregi("SUSE", $agent))
			{$bd['OSversion'] = 'SUSE';}
			elseif (eregi("Ubuntu", $agent))
			{$bd['OSversion'] = 'Ubuntu';}
			elseif (eregi("RedHat", $agent))
			{$bd['OSversion'] = 'RedHat';}
			}
			
		elseif (eregi("OS/2", $agent))
			{$bd['platform'] = "OS/2";}
		elseif (eregi("BeOS", $agent))
			{$bd['platform'] = "BeOS";}
		elseif (eregi("Wii", $agent))
			{$bd['platform'] = "Wii";}
		elseif (eregi("PSP", $agent))
			{$bd['platform'] = "PSP";}
			
		elseif (eregi("PLAYSTATION 3", $agent))
			{$bd['platform'] = "PS3";
			//format: (PLAYSTATION 3; 1.00)
			  $val = explode(" ",stristr($agent,"; "));
				$val2 = explode(")",$val[1]);
				$bd['OSversion'] = $val2[0];
			}
			elseif (eregi("Unix", $agent))
			{$bd['platform'] = "Unix";}

			//browser tests:
			
		// test for Opera		
		if (eregi("opera",$agent)){
			$val = stristr($agent, "opera");
			if (eregi("/", $val)){
				$val = explode("/",$val);
//				$bd['browser'] = $val[0];
				$bd['browser'] = 'Opera'; 
				$val = explode(" ",$val[1]);
				$bd['version'] = $val[0];
			}else{
				$val = explode(" ",stristr($val,"opera"));
//				$bd['browser'] = $val[0];
				$bd['browser'] = 'Opera'; 
				$bd['version'] = $val[1];
			}

		
		}elseif (eregi("Chrome",$agent)){
			$val = stristr($agent, "Chrome");
  		$val = explode("/",$val);
  		$bd['browser'] = 'Chrome'; 
  		$val2 = explode(" ",$val[1]);
  		$bd['version'] = $val2[0];
			
		// test for WebTV
		}elseif(eregi("webtv",$agent)){
			$val = explode("/",stristr($agent,"webtv"));
//			$bd['browser'] = $val[0];
			$bd['browser'] = 'webtv';
			$bd['version'] = $val[1];
			
			//test for Wget ( 'Wget/1.1 ')
		}elseif(eregi("Wget",$agent)){
			$val = explode("/",stristr($agent,"Wget"));
			$bd['browser'] = 'Wget';
			$val2 = explode(" ",$val[1]);
			$bd['version'] = $val2[0];
		
		// test for MS Internet Explorer version 1
		}elseif(eregi("microsoft internet explorer", $agent)){
			$bd['browser'] = "MSIE";
			$bd['version'] = "1.0";
			$var = stristr($agent, "/");
			if (ereg("308|425|426|474|0b1", $var)){
				$bd['version'] = "1.5";
			}

		// test for NetPositive
		}elseif(eregi("NetPositive", $agent)){
			$val = explode("/",stristr($agent,"NetPositive"));
			$bd['platform'] = "BeOS";
//			$bd['browser'] = $val[0];
			$bd['browser'] = 'NetPositive'; 
			$bd['version'] = $val[1];
			
		// test for Anonymouse.org
		}elseif(eregi("anonymouse", $agent)){
			$bd['browser'] = 'Anonymouse'; 
			
		// test for Charlotte bot thing
		}elseif(eregi("Charlotte", $agent)){
			$bd['browser'] = 'Bot'; 
			$val = explode("/",stristr($agent,"Charlotte"));
			$val2 = explode(";",$val[1]);
			$bd['version'] = 'Charlotte '.$val2[0];
		
		// test for Dillo
		}elseif(eregi("Dillo", $agent)){
			$val = explode("/",stristr($agent,"Dillo"));
			$bd['platform'] = "Linux";
			$bd['browser'] = 'Dillo'; 
			$bd['version'] = $val[1];

		// test for Play Station Portable
		}elseif(eregi("PSP", $agent)){
			$val = explode(" ",stristr($agent,");"));
//			$bd['platform'] = "Linux";
			$bd['browser'] = 'NetFont'; 
			$bd['version'] = $val[1];
			
		// test for MS Internet Explorer
		}elseif(eregi("msie",$agent) && !eregi("opera",$agent)){
			$val = explode(" ",stristr($agent,"msie"));
//			$bd['browser'] = $val[0];
			$bd['browser'] = 'MSIE';
			$bd['version'] = $val[1];
		
		// test for MS Pocket Internet Explorer
		}elseif(eregi("mspie",$agent) || eregi('pocket', $agent)){
			$val = explode(" ",stristr($agent,"mspie"));
			$bd['browser'] = "MSPIE";
			$bd['platform'] = "WindowsCE";
			if (eregi("mspie", $agent))
				$bd['version'] = $val[1];
			else {
				$val = explode("/",$agent);
				$bd['version'] = $val[1];
			}
			
		// test for Galeon
		}elseif(eregi("galeon",$agent)){
			$val = explode(" ",stristr($agent,"galeon"));
			$val = explode("/",$val[0]);
//			$bd['browser'] = $val[0];
			$bd['browser'] = 'Galeon';
			$bd['version'] = $val[1];
			
		// test for Konqueror
		}elseif(eregi("Konqueror",$agent)){
			$val = explode(" ",stristr($agent,"Konqueror"));
			$val = explode("/",$val[0]);
//			$bd['browser'] = $val[0];
			$bd['browser'] = 'Konqueror';
			$bd['version'] = $val[1];
			
		// test for iCab
		}elseif(eregi("icab",$agent)){
			$val = explode(" ",stristr($agent,"icab"));
//			$bd['browser'] = $val[0];
			$bd['browser'] = 'iCab'; 
			$bd['version'] = $val[1];

		// test for OmniWeb
		}elseif(eregi("omniweb",$agent)){
			$val = explode("/",stristr($agent,"omniweb"));
//			$bd['browser'] = $val[0];
			$bd['browser'] = 'Omniweb';
			$bd['version'] = $val[1];

		// test for Phoenix
		}elseif(eregi("Phoenix", $agent)){
			$bd['browser'] = "Phoenix";
			$val = explode("/", stristr($agent,"Phoenix/"));
			$bd['version'] = $val[1];
		
		// test for Firebird
		}elseif(eregi("firebird", $agent)){
			$bd['browser']="Firebird";
			$val = stristr($agent, "Firebird");
			$val = explode("/",$val);
			$bd['version'] = $val[1];
			
		// test for Firefox
		}elseif(eregi("Firefox", $agent)){
			$bd['browser']="Firefox";
			$val = stristr($agent, "Firefox");
			$val = explode("/",$val);
			$val2 = explode(" ",$val[1]);
			$bd['version'] = $val2[0];
			
			//navigator gets detected as Firefox version Navigator otherwise
			if(eregi("Navigator",$agent)){
			$val = explode(" ",stristr($agent,"Navigator"));
			$val = explode("/",$val[0]);
			$bd['browser'] = "Netscape";
			$bd['version'] = $val[1];}
			
		// test for Minefield (Firefox3)
		}elseif(eregi("Minefield", $agent)){
			$bd['browser']="Minefield";
			$val = stristr($agent, "Minefield");
			$val = explode("/",$val);
			$bd['version'] = $val[1];
						
		// test for Iceweasel (Firefox fork)
		}elseif(eregi("Iceweasel", $agent)){
			$bd['browser']="Iceweasel";
			$val = stristr($agent, "Iceweasel");
			$val = explode("/",$val);
			$bd['version'] = $val[1];

		// test for SeaMonkey
		}elseif(eregi("SeaMonkey", $agent)){
			$bd['browser']="SeaMonkey";
			$val = stristr($agent, "SeaMonkey");
			$val = explode("/",$val);
			$bd['version'] = $val[1];
			
	  // test for Mozilla Alpha/Beta Versions
		}elseif(eregi("mozilla",$agent) && 
			eregi("rv:[0-9].[0-9][a-b]",$agent) && !eregi("netscape",$agent)){
			$bd['browser'] = "Mozilla";
			$val = explode(" ",stristr($agent,"rv:"));
			eregi("rv:[0-9].[0-9][a-b]",$agent,$val);
			$bd['version'] = str_replace("rv:","",$val[0]);
			
		// test for Mozilla Stable Versions
		}elseif(eregi("mozilla",$agent) &&
			eregi("rv:[0-9]\.[0-9]",$agent) && !eregi("netscape",$agent)){
			$bd['browser'] = "Mozilla";
			$val = explode(" ",stristr($agent,"rv:"));
			eregi("rv:[0-9]\.[0-9]\.[0-9]",$agent,$val);
			$bd['version'] = str_replace("rv:","",$val[0]);
		
		// test for Lynx & Amaya
		}elseif(eregi("libwww", $agent)){
			if (eregi("amaya", $agent)){
				$val = explode("/",stristr($agent,"amaya"));
				$bd['browser'] = "Amaya";
				$val = explode(" ", $val[1]);
				$bd['version'] = $val[0];
			} else {
				$val = explode("/",$agent);
				$bd['browser'] = "Lynx";
				$bd['version'] = $val[1];
			}
		//test for Googlebot
		}elseif(eregi("Googlebot", $agent)){
			$bd['browser'] = "Bot";
			$bd['platform'] = "Linux";
			$bd['OSversion'] = "Google";
			$val = explode("/",stristr($agent,"Googlebot"));
			$val = explode(" ", $val[1]);
			$bd['version'] = "Googlebot ".$val[0];
			
		//test for msnbot
		}elseif(eregi("msnbot", $agent)){
			$bd['browser'] = "Bot";
			$bd['platform'] = "Windows";//probably, anyway
			$val = explode("/",stristr($agent,"msnbot"));
			$val = explode(" ", $val[1]);
			$bd['version'] = "MsnBot ".$val[0];
			if(eregi("media", $agent)){$bd['version']='MsnBot media'.$val[0];}
			$bd['OSversion']='MsnBot';
		//test for Yahoo's Slurp
		}elseif(eregi("Slurp", $agent)){
			$bd['browser'] = "Bot";
			$bd['platform'] = "Yahoo!";
			$bd['version']="Slurp";
					
		// test for Safari
		}elseif(eregi("safari", $agent)){
			$bd['browser'] = "Safari";
			$val = stristr($agent, "Safari");
			$val = explode("/",$val);
			$bd['version'] = $val[1];

		// test for Vienna
		}elseif(eregi("Vienna", $agent)){
			$bd['browser'] = "Vienna";
			$val = stristr($agent, "Vienna");
			$val = explode("/",$val);
			$bd['version'] = $val[1];

		// test for NetNewsWire
		}elseif(eregi("NetNewsWire", $agent)){
			$bd['browser'] = "NetNewsWire";
			$val = stristr($agent, "NetNewsWire");
			$val = explode("/",$val);
			$bd['version'] = $val[1];
			
			
		// remaining two tests are for Netscape and bots
		}elseif(eregi("netscape",$agent)){
			$val = explode(" ",stristr($agent,"netscape"));
			$val = explode("/",$val[0]);
//			$bd['browser'] = $val[0];
			$bd['browser'] = "Netscape";
			$bd['version'] = $val[1];
			
		}elseif(eregi("bot",$agent) || eregi("agent",$agent) || eregi("spider",$agent) || eregi("crawler",$agent)  || eregi("walker",$agent)){
			$bd['browser'] = "Bot";
		}
		/*elseif(eregi("mozilla",$agent) && !eregi("rv:[0-9]\.[0-9]\.[0-9]",$agent)){
			$val = explode(" ",stristr($agent,"mozilla"));
			$val = explode("/",$val[0]);
			$bd['browser'] = "Netscape";
			$bd['version'] = $val[1];
		}*/
		
		// clean up extraneous garbage that may be in the name
		$bd['browser'] = ereg_replace("[^a-z,A-Z]", "", $bd['browser']);
		// clean up extraneous garbage that may be in the version		
		$bd['version'] = ereg_replace("[^0-9,.,a-z,A-Z]", "", $bd['version']);
		
		// check for AOL
		if (eregi("AOL", $agent)){
			$var = stristr($agent, "AOL");
			$var = explode(" ", $var);
			$bd['aol'] = ereg_replace("[^0-9,.,a-z,A-Z]", "", $var[1]);
		}
		
		if(strlen($bd['version'])==0){$bd['version']='Unknown';}
		
		// finally assign our properties
		$this->Name = $bd['browser'];
		$this->Version = $bd['version'];
		$this->Platform = $bd['platform'];
		$this->AOL = $bd['aol'];
		$this->OSversion = $bd['OSversion'];
	}
}
?>
