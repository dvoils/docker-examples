<?php
session_start();
require 'settings.php';
//include 'countersilentphp.php';
require 'secure.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Contents of Hit Counter's DB Table</title>
</head>
<body>
<h2>Individual User's statistics</h2>
<form name='viewform' action='individualstats.php' method='get'>
View by <select name='viewbyip'><option value='true'>IP</option><option value='false'>Hit</option></select><br>
View <select name='viewthis'><option value='all'>All</option><option value='browser'>Where browser='unknown'</option><option value='version'>Where browser version='unknown'</option><option value='OS'>Where OS='unknown'</option><option value='OSversion'>Where OSversion='unknown'</option></select><br>
<input type='submit' value='View'>
</form>
<p>
You can click on a hit ID to see the actual HTTP_USER_AGENT.<br><br>
<script language="JavaScript" type="text/javascript">
<!--
document.viewform.viewbyip.value='<?php echo $_GET['viewbyip'] ?>';
document.viewform.viewthis.value='<?php echo $_GET['viewthis'] ?>';
//-->
</script>

<?php
$con = mysql_connect($servername, $dbusername, $dbpassword);
if (!$con){die("Could not connect:" . mysql_error());}
mysql_select_db($dbname, $con);

$ips=array();

echo "<table border='1'>";
echo "<tr><td>hit</td><td>time</td><td>IP</td><td>Browser</td><td>Version</td><td>OS</td><td>OS version</td></tr>";
$result = mysql_query("SELECT * FROM $dbtablename");
while($row = mysql_fetch_array($result))
{
$continue=true;
if($_GET['viewbyip']=='true'){if(!in_array($row['ip'], $ips)){$ips[]=$row['ip'];}else{$continue=false;}}

switch($_GET['viewthis'])
{case 'browser';if($row['browser']!=='Unknown'){$continue=false;}break;
case 'version';if($row['version']!=='Unknown'){$continue=false;}break;
case 'OS';if($row['os']!=='Unknown'){$continue=false;}break;
case 'OSversion';if($row['osversion']!=='Unknown'){$continue=false;}break;}

if($continue==true)
{
echo "\n<tr><td><span style='cursor:pointer; cursor:hand;' onclick='alert(\"".$row['useragent']."\")'>".$row['hit']."</span></td><td>".date("G\:i\, d/m/y",$row['time'])."</td><td>".$row['ip']."</td><td>".$row['browser']."</td><td>".$row['version']."</td><td>".$row['os']."</td><td>".$row['osversion']."</td></tr>";
}

}
echo "</table>";

?>
</body>
</html>
