const express = require('express');
const app = express();
const PORT = 3000;
const bodyParser = require('body-parser');

// "Middleware" required to examine body during express post and put processing
//
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.get('/', function(req, res) {
    res.json({"hello": "world"});
});


app.listen(PORT, function(){
    console.log('Your node js server is running on PORT:',PORT);
});
